/**
 * \file duma_u8.h
 * utf8 kodolasu szovegek
 *
 * LINUX alatt ez kell
 */


#define T_NEMUTF "\
A nem UTF-8 kodolast hasznal a futtato kornyezet. \
Az ekezetes szovegek lehet, hogy hibasan fognak megjelenni."

#define T_TNR   "\nTeszteset sorszáma: "
#define T_INT50 "\
default (50) elemű int tömb\n\
Adja meg a sorok számat, majd az index - érték párokat!\n"

#define T_DOUBLE100 "\
100 elemű double tömb\n\
Adja meg a sorok számat, majd az index - érték párokat!\n"

#define T_STRING11 "\
11 elemű sztring tömb saját kivetelosztállyal\n\
Adja meg a sorok számat, majd az index - sztring párokat!\n"

#define T_DUMA1 "\
A mintafeladat bemutatja az ékezetes karakterek megjelenítését is:\n\
Ez a szöveg UTF-8 kódolású:\n\
árvíztűrőtükörfúrógép ÁRVÍZTŰRŐTÜKÖRFÚRÓGÉP"

#define T_ADAT1 "\nMost beolvassuk a valami_adat.dat fájlt, ami WINX szöveg:"

#define T_ADAT2 "\nMost beolvassuk újra, de a cross-platform getline-nal:"

#define T_BEIR_TXT "Béla bácsi tűzoltó"

#define T_BEIR  "\nMost beírjuk a '" T_BEIR_TXT "' szöveget a sajat.txt fájlba."

#define T_ADAT3 "\nBeolvassuk a saját környzetben ketezett sajat.txt fájlt."

#define T_NINCS "Nics ilyen teszteset!"

#define T_SKIV  "Sajat kivétel jött."

#define T_NAGYB "*** Nagy baj van! ****"

