/**
 * \file duma_latin.h
 *
 * latin2 kodolasu szovegek
 *
 * Windows alatt ez kell
 */

#define T_NEM1250 "\
A nem 1250 kodolast hasznal a futtato kornyezet. \
Az ekezetes szovegek lehet, hogy hibasan fognak megjelenni."

#define T_TNR   "\nTeszteset sorsz�ma: "
#define T_INT50 "\
default (50) elem� int t�mb\n\
Adja meg a sorok sz�mat, majd az index - �rt�k p�rokat!\n"

#define T_DOUBLE100 "\
100 elem� double t�mb\n\
Adja meg a sorok sz�mat, majd az index - �rt�k p�rokat!\n"

#define T_STRING11 "\
11 elem� sztring t�mb saj�t kiveteloszt�llyal\n\
Adja meg a sorok sz�mat, majd az index - sztring p�rokat!\n"

#define T_DUMA1 "\
A mintafeladat bemutatja az �kezetes karakterek haszn�lat�t is.\n\
Ez a sz�veg UTF-8 k�dol�s�:\n\
�rv�zt�r�t�k�rf�r�g�p �RV�ZT�R�T�K�RF�R�G�P"

#define T_ADAT1 "\nMost beolvassuk a valami_adat.dat f�jlt, ami WINX sz�veg:"

#define T_ADAT2 "\nMost beolvassuk �jra, de a cross-platform getline-nal:"

#define T_BEIR_TXT "B�la b�csi t�zolt�"

#define T_BEIR  "\nMost be�rjuk a '" T_BEIR_TXT "' sz�veget a sajat.txt f�jlba."

#define T_ADAT3 "\nBeolvassuk a saj�t k�rnyzetben ketezett sajat.txt f�jlt."

#define T_NINCS "Nics ilyen teszteset!"

#define T_SKIV  "Sajat kiv�tel j�tt."

#define T_NAGYB "*** Nagy baj van! ****"
